﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Space.Startup))]
namespace Space
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
