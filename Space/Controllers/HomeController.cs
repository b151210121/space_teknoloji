﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace Space.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {

            return View();
        }

        public ActionResult Iletisim()
        {

            return View();
        }

        public ActionResult Firsat()
        {
            return View();
        }
        
        public ActionResult Yeni()
        {

            return View();
        }

        public ActionResult Magaza()
        {

            return View();
        }
        public ActionResult Change(string lang)
        {
                Session["lang"] = lang;
            Thread.CurrentThread.CurrentCulture = new CultureInfo(Session["lang"].ToString());
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(Session["lang"].ToString());

            return View("Index");
        }
        public ActionResult Ozellik()
        {
            return View();
        }
    }
}